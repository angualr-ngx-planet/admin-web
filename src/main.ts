import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { defineApplication, PlanetPortalApplication } from '@worktile/planet';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
//  这里需要改动 使用ng-planet
// platformBrowserDynamic().bootstrapModule(AppModule)
//   .catch(err => console.error(err));


defineApplication('admin-web',
  // {
  // template: `<app-admin-root class="app-admin-index"></app-admin-root>`,
  // bootstrap:
  (portalApp: PlanetPortalApplication) => {
    alert('来了老弟');
    return platformBrowserDynamic([
      {
        provide: PlanetPortalApplication,
        useValue: portalApp
      }
    ])
      .bootstrapModule(AppModule)
      .then(appModule => {
        return appModule;
      })
      .catch(error => {
        console.error(error);
        return null;
      });
  }
// }
);
